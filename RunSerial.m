clear; close all;



% % Choose input file to reproduce similar figures in the paper. Uncomment one of
% % them and comment out the others.
InputBerryFig1;
% InputBerryFig2L;
% InputBerryFig2M;
% InputBerryFig2R;
% InputBerryFig3and4;
% InputBerryFig5;
% InputBerrySupplementalFig2;
InputBerrySupplementalFig3;


% % Run split operator code
SplitOperator;


% % Post analysis on population growth
if TDPertTheo==1
ShowVariousData;
end